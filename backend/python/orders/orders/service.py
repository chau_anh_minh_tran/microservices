from hashids import Hashids

from nameko.events import EventDispatcher
from nameko.rpc import rpc
from nameko_sqlalchemy import DatabaseSession
from nameko.rpc import RpcProxy
from nameko.exceptions import RemoteError

from orders.exceptions import NotFound, OutOfStock
from orders.models import DeclarativeBase, Order, OrderDetail
from orders.schemas import OrderSchema

from util.Logging import init_logger


class OrdersService(object):

    name = 'orders'
    products_rpc = RpcProxy('products')

    event_dispatcher = EventDispatcher()
    db = DatabaseSession(DeclarativeBase)
    hashids = Hashids(min_length=10,
                      alphabet='0123456789abcdef',
                      salt='A secret key')

    def __init__(self):
        self.logger = init_logger(name=self.name)

    @rpc
    def getOrder(self, hashed_order_id):
        order_id = self.hashids.decrypt(hashid=hashed_order_id)
        self.logger.info("order_id: %s" % order_id)
        order = self.db.query(Order).get(order_id)

        if not order:
            raise NotFound('Order with id {} not found'.format(hashed_order_id))
        # Hide real Id from DB
        data = OrderSchema().dump(order).data
        data['id'] = hashed_order_id

        return data

    @rpc
    def createOrder(self, order_details):

        order = Order(
            order_details=[
                OrderDetail(
                    product_id=order_detail['product_id'],
                    product_name=order_detail['product_name'],
                    price=order_detail['price'],
                    quantity=order_detail['quantity'],
                    currency=order_detail['currency']
                )
                for order_detail in order_details
            ]
        )

        self.db.add(order)
        self.db.commit()

        order = OrderSchema().dump(order).data

        try:
            # self.event_dispatcher('order_created', {
            #         'order': order
            #     })
            self.products_rpc.handleOrderCreated({'order': order})
        except RemoteError as ex:
            self.logger.info("orders.createOrder: %s" % ex)
            raise OutOfStock(str(ex))
        # Hide real Id from DB
        order['hash_id'] = self.hashids.encrypt(order['id'])

        return order

    @rpc
    def updateOrder(self, order):
        order_details = {
            order_details['id']: order_details
            for order_details in order['order_details']
        }

        id = self.hashids.decrypt(order['hash_id'])
        order = self.db.query(Order).get(id)

        for order_detail in order.order_details:
            order_detail.price = order_details[order_detail.id]['price']
            order_detail.quantity = order_details[order_detail.id]['quantity']

        self.db.commit()
        return OrderSchema().dump(order).data

    @rpc
    def deleteOrder(self, order_id):
        order = self.db.query(Order).get(order_id)
        self.db.delete(order)
        self.db.commit()

    # TODO
    @rpc
    def getSpecificOrders(self, orders_list):
        pass