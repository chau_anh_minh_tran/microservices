#!/bin/bash

# Check if postgres is up and running before starting the service.

until nc -z ${POSTGRES_HOST} ${POSTGRES_PORT}; do
    echo "$(date) - waiting for postgres is up..."
    sleep 1
done


# Run the service

#nameko run --config config.yml authentication.authentication.service
python manage.py runserver