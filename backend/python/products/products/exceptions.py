class NotFound(Exception):
    pass


class OutOfStock(Exception):
    pass